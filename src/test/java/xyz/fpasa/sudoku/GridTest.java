package xyz.fpasa.sudoku;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class GridTest {
    static final int[] KNOWN_GRID = {
            0, 1, 2, 3, 4, 5, 6, 7, 8,
            9, 0, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 0, 1, 2, 3, 4, 5, 6,
            7, 8, 9, 0, 1, 2, 3, 4, 5,
            6, 7, 8, 9, 0, 1, 2, 3, 4,
            5, 6, 7, 8, 9, 0, 1, 2, 3,
            4, 5, 6, 7, 8, 9, 0, 1, 2,
            3, 4, 5, 6, 7, 8, 9, 0, 1,
            2, 3, 4, 5, 6, 7, 8, 9, 0,
    };

    @Test
    void testConstruction() throws InvalidGridException {
        // Valid values
        new Grid(KNOWN_GRID);
    }

    @Test
    void testConstructionWrongLength() {
        assertThrows(
                InvalidGridException.class,
                () -> new Grid(new int[]{1, 2, 3})
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {-5, -1, 10, 100})
    void testConstructionOutOfRange(int value) {
        int[] data = KNOWN_GRID.clone();
        data[37] = value;
        assertThrows(
                InvalidGridException.class,
                () -> new Grid(data)
        );
    }

    @Test
    void testGetCellValue() throws InvalidGridException, OutOfGridException {
        Grid grid = new Grid(KNOWN_GRID);
        assertEquals(9, grid.getCellValue(new CellRef(0, 1)));
        assertEquals(3, grid.getCellValue(new CellRef(7, 4)));
        assertEquals(0, grid.getCellValue(new CellRef(8, 8)));
        assertEquals(8, grid.getCellValue(new CellRef(8, 0)));
        assertEquals(2, grid.getCellValue(new CellRef(0, 8)));
    }

    @ParameterizedTest
    @ValueSource(ints = {-500, -1, 9, 100})
    void testGetCellValueErrors(int value) throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        assertThrows(
                OutOfGridException.class,
                () -> grid.getCellValue(new CellRef(value, 5))
        );
        assertThrows(
                OutOfGridException.class,
                () -> grid.getCellValue(new CellRef(5, value))
        );
    }

    void assertArrayEquals(int[] actual, int[] expected) throws AssertionError {
        assertEquals(actual.length, expected.length);
        for (int i = 0; i < actual.length; ++i) {
            assertEquals(actual[i], expected[i]);
        }
    }

    @Test
    void testGetRows() throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        int[][] rows = grid.getRows();
        assertArrayEquals(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8}, rows[0]);
        assertArrayEquals(new int[]{9, 0, 1, 2, 3, 4, 5, 6, 7}, rows[1]);
        assertArrayEquals(new int[]{8, 9, 0, 1, 2, 3, 4, 5, 6}, rows[2]);
        assertArrayEquals(new int[]{7, 8, 9, 0, 1, 2, 3, 4, 5}, rows[3]);
        assertArrayEquals(new int[]{6, 7, 8, 9, 0, 1, 2, 3, 4}, rows[4]);
        assertArrayEquals(new int[]{5, 6, 7, 8, 9, 0, 1, 2, 3}, rows[5]);
        assertArrayEquals(new int[]{4, 5, 6, 7, 8, 9, 0, 1, 2}, rows[6]);
        assertArrayEquals(new int[]{3, 4, 5, 6, 7, 8, 9, 0, 1}, rows[7]);
        assertArrayEquals(new int[]{2, 3, 4, 5, 6, 7, 8, 9, 0}, rows[8]);
    }

    @Test
    void testGetColumns() throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        int[][] columns = grid.getColumns();
        assertArrayEquals(new int[]{0, 9, 8, 7, 6, 5, 4, 3, 2}, columns[0]);
        assertArrayEquals(new int[]{1, 0, 9, 8, 7, 6, 5, 4, 3}, columns[1]);
        assertArrayEquals(new int[]{2, 1, 0, 9, 8, 7, 6, 5, 4}, columns[2]);
        assertArrayEquals(new int[]{3, 2, 1, 0, 9, 8, 7, 6, 5}, columns[3]);
        assertArrayEquals(new int[]{4, 3, 2, 1, 0, 9, 8, 7, 6}, columns[4]);
        assertArrayEquals(new int[]{5, 4, 3, 2, 1, 0, 9, 8, 7}, columns[5]);
        assertArrayEquals(new int[]{6, 5, 4, 3, 2, 1, 0, 9, 8}, columns[6]);
        assertArrayEquals(new int[]{7, 6, 5, 4, 3, 2, 1, 0, 9}, columns[7]);
        assertArrayEquals(new int[]{8, 7, 6, 5, 4, 3, 2, 1, 0}, columns[8]);
    }

    @Test
    void testGetBoxes() throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        int[][] boxes = grid.getBoxes();
        assertArrayEquals(new int[]{0, 1, 2, 9, 0, 1, 8, 9, 0}, boxes[0]);
        assertArrayEquals(new int[]{3, 4, 5, 2, 3, 4, 1, 2, 3}, boxes[1]);
        assertArrayEquals(new int[]{6, 7, 8, 5, 6, 7, 4, 5, 6}, boxes[2]);
        assertArrayEquals(new int[]{7, 8, 9, 6, 7, 8, 5, 6, 7}, boxes[3]);
        assertArrayEquals(new int[]{0, 1, 2, 9, 0, 1, 8, 9, 0}, boxes[4]);
        assertArrayEquals(new int[]{3, 4, 5, 2, 3, 4, 1, 2, 3}, boxes[5]);
        assertArrayEquals(new int[]{4, 5, 6, 3, 4, 5, 2, 3, 4}, boxes[6]);
        assertArrayEquals(new int[]{7, 8, 9, 6, 7, 8, 5, 6, 7}, boxes[7]);
        assertArrayEquals(new int[]{0, 1, 2, 9, 0, 1, 8, 9, 0}, boxes[8]);
    }

    @Test
    void testEquals() throws InvalidGridException {
        Grid grid1 = new Grid(KNOWN_GRID);
        Grid grid2 = new Grid(KNOWN_GRID);
        assertTrue(grid1.equals(grid2));
    }

    @Test
    void testNotEquals() throws InvalidGridException {
        int[] reversedKnownGrid = IntStream
                .range(0, KNOWN_GRID.length)
                .map(i -> KNOWN_GRID[KNOWN_GRID.length - i - 1])
                .toArray();

        Grid grid = new Grid(KNOWN_GRID);
        Grid reversedGrid = new Grid(reversedKnownGrid);
        assertFalse(grid.equals(reversedGrid));
    }

    @Test
    void testWithCell() throws InvalidGridException, OutOfGridException {
        CellRef ref = new CellRef(6, 1);

        Grid grid = new Grid(KNOWN_GRID);
        assertEquals(5, grid.getCellValue(ref));

        Grid newGrid = grid.withCell(ref, 7);
        assertEquals(5, grid.getCellValue(ref));
        assertEquals(7, newGrid.getCellValue(ref));
    }

    @ParameterizedTest
    @ValueSource(ints = {-10, -1, 9, 100})
    void testWithCellOutOfBounds(int value) throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        assertThrows(
                OutOfGridException.class,
                () -> grid.withCell(new CellRef(value, 4), 7)
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {-10, -1, 10, 100})
    void testWithCellWrongValue(int value) throws InvalidGridException {
        Grid grid = new Grid(KNOWN_GRID);
        assertThrows(
                InvalidGridException.class,
                () -> grid.withCell(new CellRef(3, 7), value)
        );
    }
}
