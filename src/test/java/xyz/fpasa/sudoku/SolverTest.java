package xyz.fpasa.sudoku;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SolverTest {
    static final int[] DUPLICATE_ROW = {
            1, 0, 0, 1, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
    };
    static final int[] DUPLICATE_COLUMN = {
            0, 0, 0, 1, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 1, 0, 0, 0, 0, 0,
    };
    static final int[] DUPLICATE_BOX = {
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 1, 0, 0,
    };

    static Stream<Grid> invalidGrids() throws InvalidGridException {
        return Stream.of(
                new Grid(DUPLICATE_ROW),
                new Grid(DUPLICATE_COLUMN),
                new Grid(DUPLICATE_BOX)
        );
    }

    @ParameterizedTest
    @MethodSource("invalidGrids")
    void testWrongInitialState(Grid grid) {
        assertThrows(
                InvalidStateException.class,
                () -> Solver.solve(grid)
        );
    }

    static final int[] SIMPLE_SUDOKU = {
            2, 0, 0, 0, 0, 0, 4, 0, 8,
            0, 0, 0, 0, 3, 4, 0, 2, 5,
            0, 7, 0, 8, 0, 0, 0, 0, 0,
            7, 0, 9, 0, 0, 0, 0, 3, 0,
            1, 0, 8, 2, 4, 0, 5, 7, 6,
            6, 0, 2, 3, 1, 7, 9, 0, 4,
            3, 6, 0, 4, 9, 0, 0, 0, 0,
            0, 2, 0, 0, 7, 8, 6, 0, 0,
            4, 0, 0, 0, 0, 0, 0, 5, 0,
    };
    static final int[] SIMPLE_SUDOKU_SOLUTION = {
            2, 9, 3, 7, 5, 1, 4, 6, 8,
            8, 1, 6, 9, 3, 4, 7, 2, 5,
            5, 7, 4, 8, 2, 6, 3, 9, 1,
            7, 4, 9, 6, 8, 5, 1, 3, 2,
            1, 3, 8, 2, 4, 9, 5, 7, 6,
            6, 5, 2, 3, 1, 7, 9, 8, 4,
            3, 6, 5, 4, 9, 2, 8, 1, 7,
            9, 2, 1, 5, 7, 8, 6, 4, 3,
            4, 8, 7, 1, 6, 3, 2, 5, 9,
    };

    @Test
    void testSolveSimple() throws
            InvalidGridException,
            OutOfGridException,
            InvalidStateException,
            NoSolutionException
    {
        Grid sudoku = new Grid(SIMPLE_SUDOKU);
        Grid solution = Solver.solve(sudoku);
        assertTrue(solution.equals(new Grid(SIMPLE_SUDOKU_SOLUTION)));
    }

    static final int[] NO_SOLUTION_SUDOKU = {
            2, 0, 0, 0, 0, 0, 4, 0, 8,
            0, 0, 0, 0, 3, 4, 0, 2, 5,
            0, 7, 1, 8, 0, 0, 0, 0, 0,
            7, 0, 9, 0, 0, 0, 0, 3, 0,
            1, 0, 8, 2, 4, 0, 5, 7, 6,
            6, 0, 2, 3, 1, 7, 9, 0, 4,
            3, 6, 0, 4, 9, 0, 0, 0, 0,
            0, 2, 0, 0, 7, 8, 6, 0, 0,
            4, 0, 0, 0, 0, 0, 0, 5, 0,
    };

    static Stream<Grid> noSolutionSudokus() throws InvalidGridException {
        return Stream.of(
                new Grid(NO_SOLUTION_SUDOKU)
        );
    }

    @ParameterizedTest
    @MethodSource("noSolutionSudokus")
    void testNoSolution(Grid grid) {
        assertThrows(
                NoSolutionException.class,
                () -> Solver.solve(grid)
        );
    }

    @Test
    void testFindEmptyCell() throws
            InvalidGridException,
            OutOfGridException
    {
        CellRef expRef = new CellRef(6, 1);

        int[] data = IntStream.generate(() -> 1).limit(81).toArray();
        Grid grid = new Grid(data).withCell(expRef, 0);

        CellRef ref = Solver.findEmptyCell(grid);
        assertEquals(expRef.x, ref.x);
        assertEquals(expRef.y, ref.y);
    }

    @Test
    void testFindEmptyCellNone() throws InvalidGridException {
        int[] data = IntStream.generate(() -> 1).limit(81).toArray();
        Grid grid = new Grid(data);
        assertEquals(null, Solver.findEmptyCell(grid));
    }
}