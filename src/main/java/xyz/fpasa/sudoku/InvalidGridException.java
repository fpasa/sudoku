package xyz.fpasa.sudoku;

public class InvalidGridException extends Exception {
    public InvalidGridException(String message) {
        super(message);
    }
}