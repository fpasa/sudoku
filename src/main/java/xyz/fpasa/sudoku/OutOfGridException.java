package xyz.fpasa.sudoku;

public class OutOfGridException extends Exception {
    public OutOfGridException(String message) {
        super(message);
    }
}