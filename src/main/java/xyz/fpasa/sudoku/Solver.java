package xyz.fpasa.sudoku;

import java.util.HashSet;
import java.util.Set;

record Solution(boolean valid, Grid grid) {}

public class Solver {
    public static Grid solve(Grid initialState) throws
            OutOfGridException,
            InvalidStateException,
            InvalidGridException,
            NoSolutionException
    {
        validateState(initialState);
        Solution solution = solveCell(initialState);
        if (!solution.valid()) {
            throw new NoSolutionException("The given Sudoku has no solution.");
        }
        return solution.grid();
    }

    static Solution solveCell(Grid state) throws OutOfGridException, InvalidGridException {
        CellRef emptyCell = findEmptyCell(state);
        if (emptyCell == null) {
            // All cells are filled
            return new Solution(true, state);
        }

        int value = 0;
        Solution solution;
        while (true) {
            if (value == 9) {
                return new Solution(false, null);
            }

            ++value;
            Grid newState = state.withCell(emptyCell, value);

            try {
                validateState(newState);
            } catch (InvalidStateException error) {
                continue;
            }

            solution = solveCell(newState);
            if (solution.valid()) {
                return solution;
            }
        }
    }

    static void validateState(Grid state) throws InvalidStateException {
        validateRows(state);
        validateColumns(state);
        validateBoxes(state);
    }

    static void validateUnique(int[][] groups, String setName) throws InvalidStateException {
        for (int i = 0; i < groups.length; ++i) {
            int[] group = groups[i];
            Set<Integer> visited = new HashSet<>();
            for (int value: group) {
                if (value == 0) continue;
                if (visited.contains(value)) {
                    throw new InvalidStateException(String.format(
                            "%s %d contains duplicate value %d",
                            setName, i, value
                    ));
                }
                visited.add(value);
            }
        }
    }

    static void validateRows(Grid state) throws InvalidStateException {
        validateUnique(state.getRows(), "Row");
    }

    static void validateColumns(Grid state) throws InvalidStateException {
        validateUnique(state.getColumns(), "Column");
    }

    static void validateBoxes(Grid state) throws InvalidStateException {
        validateUnique(state.getBoxes(), "Box");
    }

    public static CellRef findEmptyCell(Grid state) {
        int[][] rows = state.getRows();
        for (int y = 0; y < rows.length; ++y) {
            int[] row = rows[y];
            for (int x = 0; x < row.length; ++x) {
                if (row[x] == 0) {
                    return new CellRef(x, y);
                }
            }
        }
        return null;
    }

    public static void printState(Grid state) {
        int[][] rows = state.getRows();
        for (int y = 0; y < rows.length; ++y) {
            if (y != 0 && (y % 3) == 0) System.out.printf("\n");

            int[] row = rows[y];
            for (int x = 0; x < row.length; ++x) {
                if (x != 0 && (x % 3) == 0) System.out.printf(" ");
                System.out.printf("%d ", row[x]);
            }
            System.out.printf("\n");
        }

        System.out.println("---");
    }
}