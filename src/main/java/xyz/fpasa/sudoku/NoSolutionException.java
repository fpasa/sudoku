package xyz.fpasa.sudoku;

public class NoSolutionException extends Exception {
    public NoSolutionException(String message) {
        super(message);
    }
}