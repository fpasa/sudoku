package xyz.fpasa.sudoku;

import java.util.function.Function;

public class Grid {
    final int GRID_SIZE = 9;
    final int ARRAY_LENGTH = GRID_SIZE*GRID_SIZE;
    final int[] array;

    public Grid(int[] init) throws InvalidGridException {
        validateLength(init);
        validateValuesInRange(init);
        array = init;
    }

    void validateLength(int[] init) throws InvalidGridException {
        if (init.length != ARRAY_LENGTH) {
            throw new InvalidGridException(String.format(
                    "Array length must be %d (%dx%d grid).",
                    ARRAY_LENGTH, GRID_SIZE, GRID_SIZE
            ));
        }
    }

    void validateValuesInRange(int[] init) throws InvalidGridException {
        for (int i = 0; i < init.length; ++i) {
            int value = init[i];
            if (value < 0 || value > GRID_SIZE) {
                String message = String.format(
                    "Array value must be in the [0, 9] range, found %d at position %d",
                    value, i
                );
                throw new InvalidGridException(message);
            }
        }
    }

    public int getCellValue(CellRef ref) throws OutOfGridException {
        int x = ref.x;
        int y = ref.y;

        if (x < 0 || x >= GRID_SIZE || y < 0 || y >= GRID_SIZE) {
            throw new OutOfGridException(String.format(
                    "Coordinate out of grid bounds: x=%d,y=%d",
                    x, y
            ));
        }

        return array[y*GRID_SIZE + x];
    }

    int[][] collect(Function<Integer, Integer> getStride, int[] offsets) {
        int[][] groups = new int[GRID_SIZE][GRID_SIZE];
        for (int i = 0; i < GRID_SIZE; ++i) {
            for (int j = 0; j < GRID_SIZE; ++j) {
                int index = getStride.apply(i) + offsets[j];
                groups[i][j] = array[index];
            }
        }
        return groups;
    }

    public int[][] getRows() {
        return collect((i) -> i*9, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8});
    }

    public int[][] getColumns() {
        return collect((i) -> i, new int[]{0, 9, 18, 27, 36, 45, 54, 63, 72});
    }

    public int[][] getBoxes() {
        return collect(
                (i) -> (i / 3) * 27 + (i % 3) * 3,
                new int[]{0, 1, 2, 9, 10, 11, 18, 19, 20}
        );
    }

    public boolean equals(Grid other) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != other.array[i]) {
            System.out.printf("%d, %d, %d", i, array[i], other.array[i]);
                return false;
            }
        }
        return true;
    }

    public Grid withCell(CellRef ref, int value) throws OutOfGridException, InvalidGridException {
        if (ref.x < 0 || ref.x >= GRID_SIZE || ref.y < 0 || ref.y >= GRID_SIZE) {
            throw new OutOfGridException(
                    String.format("Invalid grid coordinates (%d, %d)", ref.x, ref.y)
            );
        }
        if (value < 0 || value > GRID_SIZE) {
            throw new InvalidGridException(
                    String.format("Cannot set value %d to cell", value)
            );
        }
        int[] data = array.clone();
        int index = ref.y*GRID_SIZE + ref.x;
        data[index] = value;
        return new Grid(data);
    }
}