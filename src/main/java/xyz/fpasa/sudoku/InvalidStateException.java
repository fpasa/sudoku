package xyz.fpasa.sudoku;

public class InvalidStateException extends Exception {
    public InvalidStateException(String message) {
        super(message);
    }
}