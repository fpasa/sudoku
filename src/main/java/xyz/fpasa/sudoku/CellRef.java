package xyz.fpasa.sudoku;

public class CellRef {
    public final int x, y;

    public CellRef(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
